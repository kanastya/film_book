-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Янв 28 2020 г., 16:25
-- Версия сервера: 10.3.13-MariaDB-log
-- Версия PHP: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `film_book`
--

-- --------------------------------------------------------

--
-- Структура таблицы `autors`
--

CREATE TABLE `autors` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `country_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `country`
--

INSERT INTO `country` (`id`, `country_name`) VALUES
(1, 'New Zealand'),
(2, 'USA'),
(3, 'France'),
(4, 'Italy'),
(5, 'Belgium');

-- --------------------------------------------------------

--
-- Структура таблицы `film`
--

CREATE TABLE `film` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description1` text NOT NULL,
  `img` varchar(255) NOT NULL,
  `date` year(4) NOT NULL,
  `time` varchar(50) NOT NULL,
  `id_country` int(11) NOT NULL,
  `id_producer` int(11) NOT NULL,
  `id_genre` int(11) NOT NULL,
  `description2` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `film`
--

INSERT INTO `film` (`id`, `title`, `description1`, `img`, `date`, `time`, `id_country`, `id_producer`, `id_genre`, `description2`) VALUES
(1, 'The Lord of the Rings', 'The final part of the film trilogy, based on the novel by J. R. R. Tolkien, “The Lord of the Rings,”was called “The Lord of the Rings: The Return of the King.” This film broke all records and even surpassed the Titanic, who received 11 Oscars. At the box office, the film raised more than $ 1.1 billion, and as of April 2010, it ranks third in the list of highest grossing films of all time. In addition, the film received 98 other awards and 62 nominations. Here it is!\r\n', 'http://app.loc/Public/img/film_fant/img11.jpg', 2003, '3 hours 21 min', 1, 1, 1, 'The final part of the film trilogy, based on the novel by J. R. R. Tolkien, “The Lord\r\nof the Rings,”was called “The Lord of the Rings: The Return of the King.” This film\r\nbroke all records and even surpassed the Titanic, who received 11 Oscars. At the \r\nbox office, the film raised more than $ 1.1 billion, and as of April 2010, it ranks \r\nthird in the list of highest grossing films of all time. In addition, the film received \r\n98 other awards and 62 nominations. Here it is!\r\n\r\n\r\nThe plot of the painting “The Return of the King” tells of the salvation of \r\nMiddle-earth from the power of Darkness Sauron, who sent his troops to the \r\nwalls of Minas Tirith. Under the rule of the Ring, Gollum intrigues and tries in\r\nevery possible way to quarrel the hobbits Frodo and Sam, but he does not \r\nsucceed.The final battle between the dark and light forces was on the dark side,\r\nbut could the hobbits get to the Doom Mountain in time and destroy\r\nthe Ring of Power?'),
(2, 'Iron Man 2', 'The final part of the film trilogy, based on the novel by J. R. R. Tolkien, “The Lord of the Rings,”was called “The Lord of the Rings: The Return of the King.” This film broke all records and even surpassed the Titanic, who received 11 Oscars. At the box office, the film raised more than $ 1.1 billion, and as of April 2010, it ranks third in the list of highest grossing films of all time. In addition, the film received 98 other awards and 62 nominations. Here it is!\r\n', 'http://app.loc/Public/img/film_fant/img1.jpg', 2003, '3 hours 21 min', 1, 2, 1, ''),
(3, 'The Matrix', 'The final part of the film trilogy, based on the novel by J. R. R. Tolkien, “The Lord of the Rings,”was called “The Lord of the Rings: The Return of the King.” This film broke all records and even surpassed the Titanic, who received 11 Oscars. At the box office, the film raised more than $ 1.1 billion, and as of April 2010, it ranks third in the list of highest grossing films of all time. In addition, the film received 98 other awards and 62 nominations. Here it is!\r\n', 'http://app.loc/Public/img/film_fant/img2.jpg', 2005, '3 hours 21 min', 5, 2, 1, ''),
(4, 'Paul', 'The final part of the film trilogy, based on the novel by J. R. R. Tolkien, “The Lord of the Rings,”was called “The Lord of the Rings: The Return of the King.” This film broke all records and even surpassed the Titanic, who received 11 Oscars. At the box office, the film raised more than $ 1.1 billion, and as of April 2010, it ranks third in the list of highest grossing films of all time. In addition, the film received 98 other awards and 62 nominations. Here it is!\r\n', 'http://app.loc/Public/img/film_fant/img3.jpg', 2005, '3 hours 21 min', 5, 1, 1, 'The final part of the film trilogy, based on the novel by J. R. R. Tolkien, “The Lord of the Rings,”was called “The Lord of the Rings: The Return of the King.” This film broke all records and even surpassed the Titanic, who received 11 Oscars. At the box office, the film raised more than $ 1.1 billion, and as of April 2010, it ranks third in the list of highest grossing films of all time. In addition, the film received 98 other awards and 62 nominations. Here it is!'),
(5, 'Hot Tub Time Machine', 'The final part of the film trilogy, based on the novel by J. R. R. Tolkien, “The Lord of the Rings,”was called “The Lord of the Rings: The Return of the King.” This film broke all records and even surpassed the Titanic, who received 11 Oscars. At the box office, the film raised more than $ 1.1 billion, and as of April 2010, it ranks third in the list of highest grossing films of all time. In addition, the film received 98 other awards and 62 nominations. Here it is!\r\n', 'http://app.loc/Public/img/film_fant/img4.jpg', 2005, '3 hours 21 min', 5, 1, 1, 'The final part of the film trilogy, based on the novel by J. R. R. Tolkien, “The Lord of the Rings,”was called “The Lord of the Rings: The Return of the King.” This film broke all records and even surpassed the Titanic'),
(6, 'Avatar', 'The final part of the film trilogy, based on the novel by J. R. R. Tolkien, “The Lord of the Rings,”was called “The Lord of the Rings: The Return of the King.” This film broke all records and even surpassed the Titanic, who received 11 Oscars. At the box office, the film raised more than $ 1.1 billion, and as of April 2010, it ranks third in the list of highest grossing films of all time. In addition, the film received 98 other awards and 62 nominations. Here it is!\r\n', 'http://app.loc/Public/img/film_fant/img5.jpg', 2005, '3 hours 21 min', 5, 1, 1, 'The final part of the film trilogy, based on the novel by J. R. R. Tolkien, “The Lord of the Rings,”was called “The Lord of the Rings: The Return of the King.” This film broke all records and even surpassed the Titanic'),
(7, 'Megamind', 'The final part of the film trilogy, based on the novel by J. R. R. Tolkien, “The Lord of the Rings,”was called “The Lord of the Rings: The Return of the King.” This film broke all records and even surpassed the Titanic, who received 11 Oscars. At the box office, the film raised more than $ 1.1 billion, and as of April 2010, it ranks third in the list of highest grossing films of all time. In addition, the film received 98 other awards and 62 nominations. Here it is!\r\n', 'http://app.loc/Public/img/film_fant/img6.jpg', 2005, '3 hours 21 min', 5, 1, 1, ''),
(8, 'Mars Needs Moms', 'The final part of the film trilogy, based on the novel by J. R. R. Tolkien, “The Lord of the Rings,”was called “The Lord of the Rings: The Return of the King.” This film broke all records and even surpassed the Titanic, who received 11 Oscars. At the box office, the film raised more than $ 1.1 billion, and as of April 2010, it ranks third in the list of highest grossing films of all time. In addition, the film received 98 other awards and 62 nominations. Here it is!\r\n', 'http://app.loc/Public/img/film_fant/img7.jpg', 2005, '3 hours 21 min', 5, 1, 1, 'sgagagaggggggggathshnbsfnbhsfbnsfnbsfnfgn  dftjhsdxfjndgf'),
(9, 'Hancock', 'The final part of the film trilogy, based on the novel by J. R. R. Tolkien, “The Lord of the Rings,”was called “The Lord of the Rings: The Return of the King.” This film broke all records and even surpassed the Titanic, who received 11 Oscars. At the box office, the film raised more than $ 1.1 billion, and as of April 2010, it ranks third in the list of highest grossing films of all time. In addition, the film received 98 other awards and 62 nominations. Here it is!\r\n', 'http://app.loc/Public/img/film_fant/img8.jpg', 2005, '3 hours 21 min', 5, 1, 1, 'sgagagaggggggggathshnbsfnbhsfbnsfnbsfnfgn  dftjhsdxfjndgf'),
(10, 'Back to the Future', 'The final part of the film trilogy, based on the novel by J. R. R. Tolkien, “The Lord of the Rings,”was called “The Lord of the Rings: The Return of the King.” This film broke all records and even surpassed the Titanic, who received 11 Oscars. At the box office, the film raised more than $ 1.1 billion, and as of April 2010, it ranks third in the list of highest grossing films of all time. In addition, the film received 98 other awards and 62 nominations. Here it is!\r\n', 'http://app.loc/Public/img/film_fant/img9.jpg', 2005, '3 hours 21 min', 5, 1, 1, ''),
(11, 'Men in Black', 'The final part of the film trilogy, based on the novel by J. R. R. Tolkien, “The Lord of the Rings,”was called “The Lord of the Rings: The Return of the King.” This film broke all records and even surpassed the Titanic, who received 11 Oscars. At the box office, the film raised more than $ 1.1 billion, and as of April 2010, it ranks third in the list of highest grossing films of all time. In addition, the film received 98 other awards and 62 nominations. Here it is!\r\n', 'http://app.loc/Public/img/film_fant/img10.jpg', 2005, '3 hours 21 min', 5, 1, 1, 'В сюжете фильма ужасов «Капкан» на штат Флорида обрушивается настоящее стихийное бедствие. Мощный ураган пятой категории грозит уничтожить все на своем пути поэтому объявлена срочная эвакуация. Но молодая девушка Хейли не собирается уезжать без отца, который не отвечает на звонки.'),
(12, 'The Avengers', 'The final part of the film trilogy, based on the novel by J. R. R. Tolkien, “The Lord of the Rings,”was called “The Lord of the Rings: The Return of the King.” This film broke all records and even surpassed the Titanic, who received 11 Oscars. At the box office, the film raised more than $ 1.1 billion, and as of April 2010, it ranks third in the list of highest grossing films of all time. In addition, the film received 98 other awards and 62 nominations. Here it is!\r\n', 'http://app.loc/Public/img/film_fant/img12.jpg', 2005, '3 hours 21 min', 5, 1, 1, 'В сюжете фильма ужасов «Капкан» на штат Флорида обрушивается настоящее стихийное бедствие. Мощный ураган пятой категории грозит уничтожить все на своем пути поэтому объявлена срочная эвакуация. Но молодая девушка Хейли не собирается уезжать без отца, который не отвечает на звонки.'),
(13, 'The Watch', 'The final part of the film trilogy, based on the novel by J. R. R. Tolkien, “The Lord of the Rings,”was called “The Lord of the Rings: The Return of the King.” This film broke all records and even surpassed the Titanic, who received 11 Oscars. At the box office, the film raised more than $ 1.1 billion, and as of April 2010, it ranks third in the list of highest grossing films of all time. In addition, the film received 98 other awards and 62 nominations. Here it is!\r\n', 'http://app.loc/Public/img/film_fant/img13.jpg', 2005, '3 hours 21 min', 5, 1, 1, 'В сюжете фильма ужасов «Капкан» на штат Флорида обрушивается настоящее стихийное бедствие. Мощный ураган пятой категории грозит уничтожить все на своем пути поэтому объявлена срочная эвакуация. Но молодая девушка Хейли не собирается уезжать без отца, который не отвечает на звонки.'),
(14, 'Escape from Planet Earth', 'The final part of the film trilogy, based on the novel by J. R. R. Tolkien, “The Lord of the Rings,”was called “The Lord of the Rings: The Return of the King.” This film broke all records and even surpassed the Titanic, who received 11 Oscars. At the box office, the film raised more than $ 1.1 billion, and as of April 2010, it ranks third in the list of highest grossing films of all time. In addition, the film received 98 other awards and 62 nominations. Here it is!\r\n', 'http://app.loc/Public/img/film_fant/img14.jpg', 2005, '3 hours 21 min', 5, 1, 1, 'В сюжете фильма ужасов «Капкан» на штат Флорида обрушивается настоящее стихийное бедствие. Мощный ураган пятой категории грозит уничтожить все на своем пути поэтому объявлена срочная эвакуация. Но молодая девушка Хейли не собирается уезжать без отца, который не отвечает на звонки.'),
(15, 'R.I.P.D.', 'The final part of the film trilogy, based on the novel by J. R. R. Tolkien, “The Lord of the Rings,”was called “The Lord of the Rings: The Return of the King.” This film broke all records and even surpassed the Titanic, who received 11 Oscars. At the box office, the film raised more than $ 1.1 billion, and as of April 2010, it ranks third in the list of highest grossing films of all time. In addition, the film received 98 other awards and 62 nominations. Here it is!\r\n', 'http://app.loc/Public/img/film_fant/img15.jpg', 2005, '3 hours 21 min', 5, 1, 1, 'В сюжете фильма ужасов «Капкан» на штат Флорида обрушивается настоящее стихийное бедствие. Мощный ураган пятой категории грозит уничтожить все на своем пути поэтому объявлена срочная эвакуация. Но молодая девушка Хейли не собирается уезжать без отца, который не отвечает на звонки.'),
(16, 'Edge of Tomorrow', 'The final part of the film trilogy, based on the novel by J. R. R. Tolkien, “The Lord of the Rings,”was called “The Lord of the Rings: The Return of the King.” This film broke all records and even surpassed the Titanic, who received 11 Oscars. At the box office, the film raised more than $ 1.1 billion, and as of April 2010, it ranks third in the list of highest grossing films of all time. In addition, the film received 98 other awards and 62 nominations. Here it is!\r\n', 'http://app.loc/Public/img/film_fant/img16.jpg', 2005, '3 hours 21 min', 5, 1, 1, 'В сюжете фильма ужасов «Капкан» на штат Флорида обрушивается настоящее стихийное бедствие. Мощный ураган пятой категории грозит уничтожить все на своем пути поэтому объявлена срочная эвакуация. Но молодая девушка Хейли не собирается уезжать без отца, который не отвечает на звонки.h');

-- --------------------------------------------------------

--
-- Структура таблицы `genre`
--

CREATE TABLE `genre` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `genre`
--

INSERT INTO `genre` (`id`, `title`) VALUES
(1, 'Fantasy'),
(2, 'Western'),
(3, 'Biografi'),
(4, 'Drama'),
(5, 'Comedy'),
(6, 'Detective');

-- --------------------------------------------------------

--
-- Структура таблицы `logo`
--

CREATE TABLE `logo` (
  `id` int(11) NOT NULL,
  `autor_id` int(11) NOT NULL,
  `action` varchar(20) NOT NULL,
  `cdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `logo`
--

INSERT INTO `logo` (`id`, `autor_id`, `action`, `cdate`) VALUES
(1, 5, 'Inserted', '2020-01-26 16:28:00');

-- --------------------------------------------------------

--
-- Структура таблицы `producer`
--

CREATE TABLE `producer` (
  `id` int(11) NOT NULL,
  `produser_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `producer`
--

INSERT INTO `producer` (`id`, `produser_name`) VALUES
(1, 'Peter Jacson'),
(2, 'James Cameron'),
(5, 'agdfgd sdfh');

--
-- Триггеры `producer`
--
DELIMITER $$
CREATE TRIGGER `insertLog` AFTER INSERT ON `producer` FOR EACH ROW INSERT INTO logo VALUES(null, NEW.id, 'Inserted', NOW())
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `role`
--

CREATE TABLE `role` (
  `id_role` int(11) NOT NULL,
  `role` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `role`
--

INSERT INTO `role` (`id_role`, `role`) VALUES
(1, 'user'),
(2, 'admin');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id_user`, `login`, `email`, `password`) VALUES
(1, '1234', 'johnny@pk.edu.pl', '1234'),
(3, '12345', 'dsfjsdfjslak@gmail.com', '123456'),
(4, 'ada', 'johnny@pk.edu.pl', '123'),
(5, 'ada', 'johnny@pk.edu.pl', '12345'),
(6, 'ada', 'johnny@pk.edu.pl', '$2y$10$mVKxQsWnEe94N5zKKC1SsuuRYGzSBBpMnJiEVeW71dqVnNwFbFco.'),
(7, 'dgh', 'dsfjsdfjslak@gmail.com', '$2y$10$9IaYt7x.v.vGZIxAxrloIu/hMWci9q.pIvMZkou65BvwGCap5bGeq'),
(8, 'ada', 'johnny@pk.edu.pl', '$2y$10$VRpHM42LtNvFxMwSD.6CVuzLkYdcp4CTo6bw62DkfrBPKDa77rjz2'),
(9, 'dak', 'kuras098@gmail.com', '$2y$10$Nrj525YoAc777jMY/HmaxuQTWaKcLsNF4IOG8J/XUVx8pXEX1rMp2'),
(10, 'vckv', 'johnnky@pk.edu.pl', 'eb89f40da6a539dd1b1776e522459763'),
(11, 'dak', 'johnnky@pk.edu.pl', '$2y$10$t374geMkNajuAKAS073P1.LEeqtxGk7bMKbTOLRh6IMBRP8YfJ4YS'),
(12, 'ooo', 'johnnoky@pk.edu.pl', '$2y$10$0QWl6s9L6aUeZ27Aw84gKOsGhaIGNn26LlRQuOSJ8IiI4369kAggW');

-- --------------------------------------------------------

--
-- Структура таблицы `user_role`
--

CREATE TABLE `user_role` (
  `id_user_role` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `autors`
--
ALTER TABLE `autors`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title` (`title`),
  ADD KEY `id_country` (`id_country`),
  ADD KEY `id_genre` (`id_genre`),
  ADD KEY `id_producer` (`id_producer`);

--
-- Индексы таблицы `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `logo`
--
ALTER TABLE `logo`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `producer`
--
ALTER TABLE `producer`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id_role`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `id_user` (`id_user`,`login`);

--
-- Индексы таблицы `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id_user_role`),
  ADD KEY `id_role` (`id_role`),
  ADD KEY `id_user` (`id_user`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `autors`
--
ALTER TABLE `autors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `film`
--
ALTER TABLE `film`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT для таблицы `genre`
--
ALTER TABLE `genre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `logo`
--
ALTER TABLE `logo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `producer`
--
ALTER TABLE `producer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `role`
--
ALTER TABLE `role`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id_user_role` int(11) NOT NULL AUTO_INCREMENT;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `film`
--
ALTER TABLE `film`
  ADD CONSTRAINT `film_ibfk_1` FOREIGN KEY (`id_country`) REFERENCES `country` (`id`),
  ADD CONSTRAINT `film_ibfk_2` FOREIGN KEY (`id_genre`) REFERENCES `genre` (`id`),
  ADD CONSTRAINT `film_ibfk_3` FOREIGN KEY (`id_producer`) REFERENCES `producer` (`id`);

--
-- Ограничения внешнего ключа таблицы `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`id_role`) REFERENCES `role` (`id_role`),
  ADD CONSTRAINT `user_role_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
